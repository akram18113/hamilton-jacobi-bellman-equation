import numpy as np

# Define the system dynamics and cost function
def f(x, u):
    return np.array([x[1], -x[0] - u])

def l(x, u):
    return x[0]**2 + u**2

# Define the grid and time horizon
T = 1.0
N = 100
dt = T/N
x_max = 1.0
u_max = 1.0
dx = 2*x_max/N
du = 2*u_max/N
x = np.linspace(-x_max, x_max, N+1)
u = np.linspace(-u_max, u_max, N+1)

# Initialize the value function and control policy
V = np.zeros((N+1, N+1))
pi = np.zeros((N+1, N+1))

# Iterate backward in time
for k in range(N-1, -1, -1):
    for i in range(N+1):
        for j in range(N+1):
            x_i = x[i]
            u_j = u[j]
            f_i_j = f(x_i, u_j)
            l_i_j = l(x_i, u_j)
            if i == 
